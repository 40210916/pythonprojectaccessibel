import os

import image as image
import matplotlib.image as mpimg
import matplotlib.pyplot as plt
from PIL import Image
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import tensorflow as tf
import numpy as np

from itertools import cycle
from sklearn import svm, datasets
from sklearn.metrics import roc_curve, auc
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import label_binarize
from sklearn.multiclass import OneVsRestClassifier
from scipy import interpolate
from scipy import interp
from sklearn.metrics import roc_auc_score
from tensorflow.keras.preprocessing import image


# MAIN FUNCTION
if __name__ == '__main__':
    # Directories with the training Accessible / Inaccessible pictures
    train_accessible_dir = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Training/Accessible/'
    train_inaccessible_dir = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Training/Inaccessible/'
    # Directories with the validation Accessible / Inaccessible pictures
    valid_accessible_dir = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Validation/Accessible/'
    valid_inaccessible_dir = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Validation/Inaccessible/'

    # LISTS 10 FILE NAMES FROM EACH DATASET DIRECTORY
    train_accessible_names = os.listdir(train_accessible_dir)
    print(train_accessible_names[:10])
    train_inaccessible_names = os.listdir(train_inaccessible_dir)
    print(train_inaccessible_names[:10])
    valid_accessible_names = os.listdir(valid_accessible_dir)
    print(valid_accessible_names[:10])
    valid_inaccessible_names = os.listdir(valid_inaccessible_dir)
    print(valid_inaccessible_names[:10])

    # TOTAL NUMBER OF IMAGES IN EACH DATASET DIRECTORY
    print('total training accessible images:', len(os.listdir(train_accessible_dir)))
    print('total training inaccessible images:', len(os.listdir(train_inaccessible_dir)))
    print('total validation accessible images:', len(os.listdir(valid_accessible_dir)))
    print('total validation inaccessible images:', len(os.listdir(valid_inaccessible_dir)))

    # #MATPLOT PARAMS
    # # Parameters for graph; we'll output images in a 4x4 configuration
    # nrows = 4
    # ncols = 4
    #
    # # Index for iterating over images
    # pic_index = 0
    #
    # # Set up matplotlib fig, and size it to fit 4x4 pics
    # fig = plt.gcf()
    # fig.set_size_inches(ncols * 4, nrows * 4)
    #
    # pic_index += 8
    # next_accessible_pic = [os.path.join(train_accessible_dir, fname)
    #                       for fname in train_accessible_names[pic_index - 8:pic_index]]
    # print(next_accessible_pic)
    # im = Image.open("/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Training/Accessible/000001.png")
    # im.show()
    #
    # next_inaccessible_pic = [os.path.join(train_inaccessible_dir, fname)
    #                   for fname in train_inaccessible_names[pic_index - 8:pic_index]]
    #
    # for i, img_path in enumerate(next_accessible_pic + next_inaccessible_pic):
    #     # Set up subplot; subplot indices start at 1
    #     sp = plt.subplot(nrows, ncols, i + 1)
    #     sp.axis('Off')  # Don't show axes (or gridlines)
    #
    #     img = mpimg.imread(img_path)
    #     plt.imshow(img)
    #
    # plt.show()

    # PREPROCESSING IMAGES

    # All images will be rescaled by 1./255
    train_datagen = ImageDataGenerator(rescale=1 / 255)
    validation_datagen = ImageDataGenerator(rescale=1 / 255)

    # Flow training images in batches of 120 using train_datagen generator
    train_generator = train_datagen.flow_from_directory(
        '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Training/',  # This is the source directory for training images
        classes=['Accessible', 'Inaccessible'],
        target_size=(200, 200),  # All images will be resized to 200x200
        batch_size=100,
        # Use binary labels
        class_mode='binary')

    # Flow validation images in batches of 19 using valid_datagen generator
    validation_generator = validation_datagen.flow_from_directory(
        '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Validation',  # This is the source directory for training images
        classes=['Accessible', 'Inaccessible'],
        target_size=(200, 200),  # All images will be resized to 200x200
        batch_size=10,
        # Use binary labels
        class_mode='binary',
        shuffle=False)

model = tf.keras.models.Sequential([tf.keras.layers.Flatten(input_shape=(200, 200, 3)),
                                    tf.keras.layers.Dense(128, activation = tf.nn.relu),
                                    tf.keras.layers.Dense(1, activation = tf.nn.sigmoid)])

# model.summary()

model.compile(optimizer = tf.optimizers.Adam(),
              loss = 'binary_crossentropy',
              metrics = ['accuracy'])

# TRAINING

history = model.fit(train_generator,
                    steps_per_epoch=8,
                    epochs=10,
                    verbose=1,
                    validation_data= validation_generator,
                    validation_steps=1)

model.evaluate(validation_generator)
STEP_SIZE_TEST = validation_generator.n//validation_generator.batch_size
validation_generator.reset()
preds = model.predict(validation_generator, verbose=1)

fpr, tpr, _ = roc_curve(validation_generator.classes, preds)
roc_auc = auc(fpr, tpr)
plt.figure()
lw = 2
plt.plot(fpr, tpr, color='darkorange',
         lw=lw, label='ROC curve (area = %0.2f' % roc_auc)
plt.plot([0,1], [0,1], color='navy', lw=lw, linestyle="--")
plt.xlim([0.0, 1.0])
plt.ylim([0.0, 1.05])
plt.xlabel('False Positive Rate')
plt.ylabel("True Positive Rate")
plt.title("Receiver Operating Characteristic")
plt.legend(loc="lower right")
plt.show()


directory_path = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/'
print(f'Reading from: {directory_path} directory')
paths = []
counter = 0
len_of_directory = len(os.listdir(directory_path))
# print(f'There are {len_of_directory} files in the directory')

for file in os.listdir(directory_path):
    current_path_to_file = os.path.join(directory_path, file)
    paths.append(current_path_to_file)
    counter = counter +1
    print(current_path_to_file)

    print(f'Collected {counter} paths from provided directory')
    # print(f'{paths}')

for file in os.listdir(directory_path):
    #predicting images
    # path = os.listdir(directory_path) + file
    path = ['/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/1.png']
    print(current_path_to_file)
    print(path)
    img = image.load_img(current_path_to_file, target_size=(200, 200))
    x = image.img_to_array(img)
    plt.imshow(x/255.)
    x = np.expand_dims(x, axis=0)
    images = np.vstack([x])
    classes = model.predict(images, batch_size=10)
    print(classes[0])
    if classes[0]<0.5:
        print(file + " is Accessible")
    else:
        print(file + " is Inaccessible")

# directory_path1 = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/1.png'
imgg = image.load_img('/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/1.png', target_size=(200, 200))
blah = model.predict(imgg, batch_size=10)
print(blah)

# path = ['/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/1.png']
current_path_to_file = '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Unseen/7.png'
print(current_path_to_file)
# print(path)
img = image.load_img(current_path_to_file, target_size=(200, 200))
x = image.img_to_array(img)
plt.imshow(x/255.)
x = np.expand_dims(x, axis=0)
images = np.vstack([x])
classes = model.predict(images, batch_size=10)
print(classes[0])
if classes[0]<0.5:
    print(current_path_to_file + " is Accessible")
else:
    print(current_path_to_file + " is Inaccessible")


