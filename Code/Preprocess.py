# # ----------------------- PRE-PROCESSING STAGE -----------------------
#
# from tensorflow.keras.preprocessing.image import ImageDataGenerator
# #
# # All images will be rescaled by 1./255
# train_datagen = ImageDataGenerator(rescale=1 / 255)
# validation_datagen = ImageDataGenerator(rescale=1 / 255)
#
# # Flow training images in batches of 120 using train_datagen generator
# train_generator = train_datagen.flow_from_directory(
#     '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Training/',
#     # This is the source directory for training images
#     classes=['Accessible', 'Inaccessible'],
#     target_size=(200, 200),  # All images will be resized to 200x200
#     batch_size=120,
#     # Use binary labels
#     class_mode='binary')
#
# # Flow validation images in batches of 19 using valid_datagen generator
# validation_generator = validation_datagen.flow_from_directory(
#     '/Users/kathe/Documents/Final Year/CSC3002 Computer Science Project/Accessibel/Dataset/Validation',
#     # This is the source directory for training images
#     classes=['Accessible', 'Inaccessible'],
#     target_size=(200, 200),  # All images will be resized to 200x200
#     batch_size=10,
#     # Use binary labels
#     class_mode='binary',
#     shuffle=False)